# Russell, S., & Norvig, P. (2010). Artificial Intelligence A Modern Approach Third Edition. In
# Pearson. https://doi.org/10.1017/S0269888900007724

from math import inf


def minimax_decision(state, game):
    """Given a _state in a game, calculate the best move by searching
    forward all the way to the terminal states. [Figure 5.3]"""

    player = game.to_move(state)

    def max_value(_state):
        if game.terminal_test(_state):
            return game.utility(_state, player)
        v = -inf
        for a in game.actions(_state):
            v = max(v, min_value(game.result(_state, a)))
        return v

    def min_value(_state):
        if game.terminal_test(_state):
            return game.utility(_state, player)
        v = inf
        for a in game.actions(_state):
            v = min(v, max_value(game.result(_state, a)))
        return v

    # Body of minimax_decision:
    return max(game.actions(state),
               key=lambda a: min_value(game.result(state, a)))
