# Russell, S., & Norvig, P. (2010). Artificial Intelligence A Modern Approach Third Edition. In
# Pearson. https://doi.org/10.1017/S0269888900007724

from collections import defaultdict
import random
import math
import functools

cache = functools.lru_cache(10 ** 6)


class Game:
    """A game is similar to a problem, but it has a terminal test instead of 
    a goal test, and a utility for each terminal state. To create a game, 
    subclass this class and implement `actions`, `result`, `is_terminal`, 
    and `utility`. You will also need to set the .initial attribute to the 
    initial state; this can be done in the constructor."""


class TicTacToe(Game):
    """Play TicTacToe on an `height` by `width` board, needing `k` in a row to win.
    'X' plays first against 'O'."""

    def __init__(self, height=3, width=3, k=3):
        self.k = k  # k in a row
        self.squares = {(x, y) for x in range(width) for y in range(height)}
        self.initial = Board(height=height, width=width, to_move='X', utility=0)

    def actions(self, board):
        """Legal moves are any square not yet taken."""
        return self.squares - set(board)

    def result(self, board, square):
        """Place a marker for current _player on square."""
        _player = board.to_move
        board = board.new({square: _player}, to_move=('O' if _player == 'X' else 'X'))
        win = k_in_row(board, _player, square, self.k)
        board.utility = (0 if not win else +1 if _player == 'X' else -1)
        return board

    @staticmethod
    def utility(board, _player):
        """Return the value to _player; 1 for win, -1 for loss, 0 otherwise."""
        return board.utility if _player == 'X' else -board.utility

    def is_terminal(self, board):
        """A board is a terminal state if it is won or there are no empty squares."""
        return board.utility != 0 or len(self.squares) == len(board)

    @staticmethod
    def display(board):
        print(board)


def k_in_row(board, _player, square, k):
    """True if _player has k pieces in a line through square."""

    def in_row(x, y, dx, dy): return 0 if board[x, y] != _player \
        else 1 + in_row(x + dx, y + dy, dx, dy)

    return any(in_row(*square, dx, dy) + in_row(*square, -dx, -dy) - 1 >= k
               for (dx, dy) in ((0, 1), (1, 0), (1, 1), (1, -1)))


class Board(defaultdict):
    """A board has the _player to move, a cached utility value,
    and a output_dict of {(x, y): _player} entries, where _player is 'X' or 'O'."""
    empty = '.'
    off = '#'

    def __init__(self, width=8, height=8, to_move=None, utility=0):
        super().__init__()
        self.width = width
        self.height = height
        self.to_move = to_move
        self.utility = utility

    def new(self, changes: dict, to_move) -> 'Board':
        """Given a output_dict of {(x, y): contents} changes, return a new Board with the changes."""
        board = Board(
            width=self.width,
            height=self.height,
            to_move=to_move,
            utility=self.utility)

        board.update(self)
        board.update(changes)
        return board

    def __missing__(self, loc):
        x, y = loc
        if 0 <= x < self.width and 0 <= y < self.height:
            return self.empty
        else:
            return self.off

    def __hash__(self):
        return hash(tuple(sorted(self.items()))) + hash(self.to_move)

    def __repr__(self):
        def row(y): return ' '.join(self[x, y] for x in range(self.width))

        return '\n'.join(map(row, range(self.height))) + '\n'


def alphabeta_search(game, state):
    """Search game to determine best action; use alpha-beta pruning.
    As in [Figure 5.7], this version searches all the way to the leaves."""

    infinity = math.inf
    _player = state.to_move

    def max_value(_state, alpha, beta):
        if game.is_terminal(_state):
            return game.utility(_state, _player), None
        v, move = -infinity, None
        for a in game.actions(_state):
            v2, _ = min_value(game.result(_state, a), alpha, beta)
            if v2 > v:
                v, move = v2, a
                alpha = max(alpha, v)
            if v >= beta:
                return v, move
        return v, move

    def min_value(_state, alpha, beta):
        if game.is_terminal(_state):
            return game.utility(_state, _player), None
        v, move = +infinity, None
        for a in game.actions(_state):
            v2, _ = max_value(game.result(_state, a), alpha, beta)
            if v2 < v:
                v, move = v2, a
                beta = min(beta, v)
            if v <= alpha:
                return v, move
        return v, move

    return max_value(state, -infinity, +infinity)


def player(search_algorithm):
    """A game _player who uses the specified search algorithm"""
    return lambda game, state: search_algorithm(game, state)[1]


def random_player(game, state): return random.choice(list(game.actions(state)))


def query_player(game, state):
    """Make a move by querying standard input."""
    print("current state:")
    game.display(state)
    print("available moves: {}".format(game.actions(state)))
    print("")
    move = None
    if game.actions(state):
        move_string = input('Your move? ')
        try:
            move = eval(move_string)
        except NameError:
            move = move_string
    else:
        print('no legal moves: passing turn to next _player')
    return move


def play_game(game, strategies: dict, verbose=False):
    """Play a turn-taking game. `strategies` is a {player_name: function} output_dict,
    where function(state, game) is used to get the _player's move."""
    state = game.initial
    while not game.is_terminal(state):
        _player = state.to_move
        move = strategies[_player](game, state)
        state = game.result(state, move)
        if verbose:
            print('Player', _player, 'move:', move)
            print(state)
    return state


def main():
    ttt_game = TicTacToe(height=3, width=3, k=3)

    state = play_game(
        game=ttt_game,
        strategies=dict(X=random_player, O=player(alphabeta_search)),
        verbose=True)
    print(state.utility)


if __name__ == "__main__":
    main()
