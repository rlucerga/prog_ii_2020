from typing import Tuple

student_tuples = [
    ('john', 'A', 15),
    ('jane', 'B', 12),
    ('dave', 'B', 10),
]

print(sorted(student_tuples, key=lambda student: student[2]))
print(sorted(student_tuples, key=lambda student: (student[1], student[2])))


def sorting_criteria(x: Tuple[str, str, int]) -> int:
    return x[2]


print(sorted(student_tuples, key=sorting_criteria))


class User:
    def __init__(self, name: str, age: int):
        self.name = name
        self.age = age

    def __repr__(self):
        return f"User('{self.name}', {self.age})"


user_list = [User('john', 47), User('peter', 33), User('mary', 10)]

sorted(user_list, key=lambda u: u.age)