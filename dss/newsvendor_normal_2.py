import numpy as np
from itertools import repeat
import json

PRICE = 7
PRICE_2 = 4
UNIT_COST = 5
N = 10000


def deterministic_model(q: float, demand: float) -> float:
    revenue = PRICE * min(q, demand) + PRICE_2 * (q - min(q, demand))
    cost = UNIT_COST * q
    return revenue - cost

def sim_demand():
    mux = 50
    sigmax = 10
    return np.maximum(np.random.normal(mux, sigmax, size=N), 0)


def monte_carlo():
    demand = sim_demand()

    results = {}

    for q in range(30, 70):
        # Expected revenue
        profit = map(deterministic_model, repeat(q, N), demand)
        e_profit = sum(profit) / N

        results.update({q: round(e_profit, 2)})

    return results


if __name__ == '__main__':
    sim_results = monte_carlo()
    best_q = max(sim_results, key=sim_results.get)
    print(best_q)

    with open('newsvendor.json', 'w') as f:
        json.dump(sim_results, f, sort_keys=True, indent=4)