# Adapted from https://www.python-course.eu/input.php

# There are hardly any programs without any input. Input can come in various ways, for example
# from a database, another computer, mouse clicks and movements or from the internet. Yet,
# in most cases the input stems from the keyboard. For this purpose, Python provides the function
# input(). input has an optional parameter, which is the prompt string.

name = input("What's your name? ")
print(f"Nice to meet you {name}!")
age = input("Your age? ")
print(f"So, you are already {age} years old, {name}!")

colours = input("Your favourite colours? ")
# ["red","green","blue"]
print(type(colours))

colours = input("Your favourite colours? ")
# ["red","green","blue"]
print(type(eval(colours)))