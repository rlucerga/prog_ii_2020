import torch

u_1 = torch.tensor([2.], requires_grad=True)
u_2 = torch.tensor([3.], requires_grad=True)
u_3 = u_1 + u_2
"""
u_3
Out[5]: tensor([5.], grad_fn=<AddBackward0>)
"""

u_3.retain_grad()
u_4 = u_2 * u_3
# 1.2.3 Example 2, the forward pass algorithm

print(u_4)

# Example 4: backpropagation
print(u_1.data)
# tensor([2.])
print(u_1.grad)
# None
print(u_3.grad_fn)
# <AddBackward0 at 0x7fb123044be0>
print(u_4.grad_fn)
# <MulBackward0 at 0x7fb123044040>

# P4, initialization, gradient w.r.t itself
p_4 = torch.tensor([1.])

# Specify retain_graph=True if you need to backward through the graph a second time or if
# you need to access saved variables after calling backward.

u_4.backward(gradient=p_4, retain_graph=True)

print(u_1.grad)
# tensor([3.])
print(u_2.grad)
# tensor([8.])
print(u_3.grad)
# tensor([3.])
