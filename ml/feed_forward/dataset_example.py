from torch.utils.data.dataset import Dataset


class MyList(Dataset):
    def __init__(self, data: list, labels: list):
        self.data = data
        self.labels = labels

    def __len__(self):
        return len(self.data)

    def __getitem__(self, item: int):
        return self.data[item], self.labels[item]



d = MyList([1, 3, 4, 5], [1, 3, 4, 5])
