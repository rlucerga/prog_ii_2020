from functools import reduce

class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age

  def __repr__(self):
    return 'Person(' + self.name + ', ' + str(self.age) + ')'


data = [Person('John', 54), Person('Phoebe', 21), Person('Adam', 19)]
total_age = reduce(lambda running_total, person: running_total + person.age, data, 0)
average_age = total_age // len(data)
print('Average age:', average_age)
# Alternatve with generator expresssion
total_age_alterative = sum(person.age for person in data)

# total_age = reduce(lambda x, y: x + y, data, 0)

def f(running_total, person):
    return running_total + person.age


total_age_2 = reduce(f, data, 0)

# y_1 = f(0, primer elemento de data)
# y_2 = f(y_1, segundo elemento de data)
# y_3 = f(y_2, tercer elemento de data)
# devuelve y_3


# 54 = f(0, Person('John', 54))
# 75 = f(y_1, Person('Phoebe', 21))
# 94 = f(y_2, Person('Adam', 19))
# devuelve 94

sum([p.age for p in data])