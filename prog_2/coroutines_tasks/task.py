import asyncio
import datetime as dt

async def slow():
    print('slow starts')
    await asyncio.sleep(3)
    print('slow ends')

async def fast():
    print('quick starts')
    await asyncio.sleep(1)
    print('quick ends')

async def main():
    task1 = asyncio.create_task(
        slow())

    task2 = asyncio.create_task(
        fast())

    await task1
    await task2
    print(f"finished at {dt.datetime.now().strftime('%X')}")

asyncio.run(main())
