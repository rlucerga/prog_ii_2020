from multiprocessing import Process, Pipe
from time import sleep

def worker(conn):
    print('Worker - started now sleeping for 1 second')
    sleep(1)
    print('Worker - sending data via Pipe')
    conn.send('hello')
    print('Worker - closing worker end of connection')
    conn.close()

def main():
    print('Main - starting, creating the Pipe')
    main_connection, worker_connection = Pipe()
    print('Main- setting up de processs')
    p = Process(target=worker, args=(worker_connection,))
    print('Main - starting the process')
    p.start()
    print('Main - wait for a response from the child process')
    print(main_connection.recv())
    print('Main - closing parent process end of connection')
    main_connection.close()
    print('Main - Done')

if __name__ == '__main__':
    main()