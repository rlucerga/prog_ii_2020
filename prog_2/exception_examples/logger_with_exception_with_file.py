import math
import logging.config
import logging
import yaml

with open('logging.config_2.yaml', 'r') as f:
    config = yaml.safe_load(f.read())
    logging.config.dictConfig(config)

logger = logging.getLogger('myLogger')

def calc_log(x):
    logger.debug('calling calc_log')
    return math.log(x)


try:
    calc_log(-2)
    logger.info('Done')
except ValueError as e:
    logger.error(e)

