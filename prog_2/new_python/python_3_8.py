from datetime import date

# Assignment expressions

a = [i for i in range(0, 20)]

if (n := len(a)) > 10:
    print(f"List is too long ({n} elements, expected <= 10)")


# Positional only paramters

# In the following example, parameters a and b are positional-only, while c or d can be
# positional or keyword, and e or f are required to be keywords:

def f(a, b, /, c, d, *, e, f):
    print(a, b, c, d, e, f)


# f strings (already since 3.6, already extended)

user = 'eric_idle'
member_since = date(1975, 7, 31)
print(f'{user=} {member_since=}')

# https://docs.python.org/3.8/reference/lexical_analysis.html#f-strings
# The usual f-string format specifiers allow more control over how the result of the expression
# is displayed:

delta = date.today() - member_since
f'{user=!s}  {delta.days=:,d}'
