import requests
import time
import concurrent.futures
import os

img_urls = [
    'http://ipv4.download.thinkbroadband.com/50MB.zip',
    'http://ipv4.download.thinkbroadband.com/20MB.zip'
]


def download_image(img_url):
    img_bytes = requests.get(img_url).content
    img_name = img_url.split('/')[3]
    img_name = f'{img_name}'
    with open(os.path.join('images', img_name), 'wb') as img_file:
        img_file.write(img_bytes)

    return f'{img_name} was downloaded...'


if __name__ == '__main__':
    t1 = time.perf_counter()

    # Encapsulates the asynchronous execution of a callable. Future instances are created by
    # Executor.submit()
    with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
        print(f'Downloading {img_urls[0]}...')
        f1 = executor.submit(download_image, img_urls[0])
        print(f'Downloading {img_urls[1]}...')
        f2 = executor.submit(download_image, img_urls[1])

    # Return the value returned by the call. If the call hasn’t yet completed then this method
    # will wait up to timeout seconds.
    print(f1.result(timeout=10))  # 0.1 returns TimeOutError
    print(f2.result())

    t2 = time.perf_counter()

    print(f'Finished in {t2-t1} seconds')
