import requests
import time
import os

img_urls = [
    'http://ipv4.download.thinkbroadband.com/20MB.zip',
    'http://speedtest4.tele2.net/100MB.zip',
]

img_urls = [
    'https://speed.hetzner.de/100MB.bin',
    'https://speed.hetzner.de/100MB.bin',
]

def download_image(img_url):
    img_bytes = requests.get(img_url).content
    img_name = img_url.split('/')[3]
    img_name = f'{img_name}'
    with open(os.path.join('images', img_name), 'wb') as img_file:
        img_file.write(img_bytes)
        print(f'{img_name} was downloaded...')


if __name__ == '__main__':
    t1 = time.perf_counter()

    for image in img_urls:
        print(f'Downloading {image}...')
        download_image(image)

    t2 = time.perf_counter()

    print(f'Finished in {t2-t1} seconds')


"""
Large File (200MB)

IPv4 Port: 80, 81, 8080
IPv6 Port: 80, 81, 8080

14 mins @ 2 Mbps
4 mins @ 8 Mbps
1 mins @ 30 Mbps
27 secs @ 60 Mbps
14 secs @ 120 Mbps

Small File (20MB)

IPv4 Port: 80, 81, 8080
IPv6 Port: 80, 81, 8080

80 secs @ 2 Mbps
20 secs @ 8 Mbps
6 secs @ 30 Mbps
3 secs @ 60 Mbps
1.5 secs @ 120 Mbps

Medium File (100MB)

IPv4 Port: 80, 81, 8080
IPv6 Port: 80, 81, 8080

8 mins @ 2 Mbps
2 mins @ 8 Mbps
27 secs @ 30 Mbps
14 secs @ 60 Mbps
7 secs @ 120 Mbps
"""

from collections import namedtuple
import numpy as np

Statistics = namedtuple('Statistics', ['mean', 'std', 'max', 'min'])


def statistics(v: np.ndarray) -> Statistics:
    return Statistics(v.mean(), v.std(), max(v), min(v))


r = statistics(np.array([1, 2, 3, 4, 5]))

print(f'Mean: {r[0]}')
