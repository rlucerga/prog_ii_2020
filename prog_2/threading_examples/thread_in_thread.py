from threading import Thread
from time import sleep


def worker_in():
    for i in range(0, 10):
        print('*', end='', flush=True)
        sleep(0.2)


def worker_out():
    for i in range(0, 10):
        print('-', end='', flush=True)
        sleep(0.2)
    tin = Thread(target=worker_in, name='in')
    # Start a new thread with worker in
    tin.start()
    for i in range(0, 10):
        print('.', end='', flush=True)
        sleep(0.2)


print('Starting')
tout = Thread(target=worker_out, name='out')
tout.start()
tout.join()

print('\nDone')
