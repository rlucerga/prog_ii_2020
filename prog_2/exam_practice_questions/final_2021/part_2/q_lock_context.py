from multiprocessing import Process, Lock
from time import sleep


def f(lk, i):
    with lk:
        print('API call process:', i)
        sleep(2)
        print('API return to process:', i)


if __name__ == '__main__':
    lock = Lock()

    for num in range(4):
        Process(target=f, args=(lock, num)).start()
