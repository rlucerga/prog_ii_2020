from multiprocessing import Process, Lock, set_start_method
from time import sleep


def f(lk, i):
    with lk:
        print('API call process:', i)
        sleep(2)
        print('API return to process:', i)


if __name__ == '__main__':
    set_start_method('fork')

    lock = Lock()

    for num in range(4):
        Process(target=f, args=(lock, num)).start()
