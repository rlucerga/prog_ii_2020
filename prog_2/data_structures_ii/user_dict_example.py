import datetime as dt
from time import sleep
from collections import UserDict


class MonitoredDict(UserDict):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._key_utc = {}
        self.data = {}

    def __setitem__(self, key, value):
        self.data[key] = value
        self._key_utc[key] = dt.datetime.utcnow()

    @property
    def key_utc(self):
        return self._key_utc


d = MonitoredDict()
d['a'] = 1
sleep(2)
d['b'] = 1

print(list(d.values()))

print(d)
print(d.key_utc)
