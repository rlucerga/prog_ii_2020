import re
from collections import Counter

# Tally occurrences of words in a list
cnt = Counter(['red', 'blue', 'red', 'green', 'blue', 'blue'])
cnt['red'] += 1

print(cnt)
cnt2 = Counter(['red', 'blue', 'red'])

print(cnt + cnt2)

# Find the ten most common words in Hamlet

words = re.findall(r'\w+', open('hamlet.txt').read().lower())
print(Counter(words).most_common(7))

# count of a missing element is zero
c = Counter(['eggs', 'ham'])
c['bacon']