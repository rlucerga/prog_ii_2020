import dataclasses


@dataclasses.dataclass
class Power:
    n: int

    def __call__(self, a: int) -> int:
        return a ** self.n


p = Power(3)
q = Power(5)
print(p(4))

