def generate_ints(N):
    for i in range(N):
        yield i


it_func = generate_ints(2)

print(next(it_func))
print(next(it_func))
try:
    print(next(it_func))
except StopIteration:
    print('end')


def gen_numbers():
    print('Start')
    yield 1
    print('Continue')
    yield 2
    print('Final')
    yield 3
    print('End')


for i in gen_numbers():
    print(i)
