def f(accum=0, m=10):
    for i in range(m):
        rcv = yield accum
        if rcv is None:
            accum -= 1
        else:
            accum = rcv


g = f(10, 8)
print(next(g))
print(next(g))
print(next(g))
print(g.send(10))
print(next(g))
print(next(g))
print(next(g))
