import logging.config

# Logging and configuration
log_file_path = "prog_2/logger_examples/logger.conf"
logging.config.fileConfig(log_file_path)
logger = logging.getLogger('sync')


def f():
    logger.info('Printing hello')
    print('hello')


f()
