from dataclasses import dataclass
from math import log2


@dataclass
class DomainError(Exception):
    message: str


@dataclass
class FloatError(Exception):
    message: str


def protected_log2() -> float:
    try:
        x_f = float(input('Input a number: '))
    except ValueError as exp:
        raise FloatError from exp

    try:
        y = log2(x_f)
    except ValueError:
        raise DomainError

    return y


try:
    print(protected_log2())
except DomainError:
    print('error in domain, cannot be negative')
except FloatError:
    print('error in domain, string cannot be converted to float')

