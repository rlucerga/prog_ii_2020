from math import log2


def f(x: float) -> float:
    return log2(x) + 1


def g():
    try:
        y = f('a')
        print(y)
    except ValueError:
        print('log domain error')
    except TypeError:
        print('log type error')


g()



