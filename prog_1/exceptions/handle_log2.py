from math import log2


while True:
    try:
        x_f = float(input('Input a number: '))
    except ValueError:
        print('Invalid input, cannot convert to float')
    else:
        try:
            print(log2(x_f))
        except ValueError:
            print('Invalid input, domain error')
        else:
            break

