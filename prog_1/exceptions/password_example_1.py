import re

class InvalidPassword(Exception):
    def __init__(self, message: str):
        self.message = message

    def __repr__(self):
        return f'InvalidPassword(\'{self.message}\')'

def password_checker(pwd: str) -> bool:

    if len(pwd) < 8:
        raise InvalidPassword('Insufficient Length')

    if len(re.findall(r'([a-zñç])', s)) < 1:
        return False

    if len(re.findall(r'([A-ZÑ])', s)) < 1:
        return False

    if len(re.findall(r'(\d)', s)) < 1:
        return False

    pattern = re.compile(r'[^a-zA-Z\dÑñç]')

    if len(re.findall(pattern, s)) < 1:
        return False

    return True


s = "R@ñm@_f0rtu9e$"
password_checker('aZ1')