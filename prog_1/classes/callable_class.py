import math


class Sigmoid(object):
    def __init__(self, weight: float, threshold: float):
        self.w = weight
        self.t = threshold

    def __call__(self, x):
        return 1 / (1 + math.exp(- self.w * (x - self.t)))


sig = Sigmoid(5, 1)  # sig is now callable, like a function
sig(1)  # calling the instance
