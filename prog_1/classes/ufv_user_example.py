class User:
    domain = '@ufv.es'
    teacher_domain = '@profesor.ufv.es'

    def __init__(self, name: str, email: str):
        self.name = name
        self.email = email

    @classmethod
    def ufv_user(cls, name: str):
        return cls(name, name + cls.domain)

    @classmethod
    def ufv_teacher(cls, name: str):
        return cls(name, name + cls.teacher_domain)

    @classmethod
    def from_dict(cls, d: dict[str, str]) -> 'User':
        return cls(d['name'], d['email'])


mongo = {'name': 'juan', 'email': 'juan@ufv.es'}

u1 = User('pepe', 'pepe@ufv.es')
u2 = User.ufv_user('juan')
u3 = User.from_dict(mongo)
