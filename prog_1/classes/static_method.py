from math import pi

class Circle:
    def __init__(self, radius: float):
        self.r = radius

    def properties(self):
        return {'area': Circle.area(self.r),
                'perimiter': Circle.perimeter(self.r)}

    @staticmethod
    def area(r: float):
        return round(pi * r ** 2, 2)

    @staticmethod
    def perimeter(r: float):
        return round(2 * pi * r, 2)


c = Circle(3)
print(c.properties())