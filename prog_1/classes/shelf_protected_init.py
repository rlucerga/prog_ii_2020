class Publication:
    def __init__(self, title: str, author: str):
        self.title = title
        self.author = author

    def __str__(self):
        return f'{self.title} by {self.author}'


class Book(Publication):
    def __init__(self, title: str, author: str, chapters: list):
        super().__init__(title, author)
        self.chapters = chapters


class Magazine(Publication):
    def __init__(self, title: str, author: str, image_list: list):
        super().__init__(title, author)
        self.image_list = image_list


class Bookshelf:
    def __init__(self):
        self._shelf = {}


bookshelf = Bookshelf()