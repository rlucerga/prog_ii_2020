import datetime as dt


class WayPoint:
    def __init__(self, lat: float, lon: float, time: dt.datetime):
        self.lat = lat
        self.lon = lon
        self.time = time


class RouteSteps:
    def __init__(self, driver: str, wp: WayPoint):
        self.driver = driver
        self.wp = wp

class Delivery(RouteSteps):
    def __init__(self, driver: str, wp: WayPoint, receiver: str):
        super().__init__(driver, wp)
        self.receiver = receiver


class Pickup(RouteSteps):
    def __init__(self, driver: str, wp: WayPoint, sender: str):
        super().__init__(driver, wp)
        self.sender = sender


class Report:
    def __init__(self, report: str, level: int, wp: WayPoint):
        self.report = report
        self.level = level
        self.wp = wp


d = Delivery("Pepe", WayPoint(40.3, 2.71, dt.datetime(2021, 1, 1)), "Juan")
