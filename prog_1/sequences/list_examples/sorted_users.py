from dataclasses import dataclass
import random
import names


@dataclass
class User:
    """An application user object"""
    name: str
    age: int

    @classmethod
    def create_random_user(cls) -> 'User':
        return cls(names.get_full_name(),
                   random.randint(0, 100))

    @classmethod
    def random_user_gen(cls, limit: int) -> list['User']:
        user_list = []
        for _ in range(limit):
            user_list.append(cls.create_random_user())
        return user_list


# A random name and age
print(names.get_full_name())
print(random.randint(0, 100))

# A random user
u = User.create_random_user()

# A random list of users
users = User.random_user_gen(10)

# Sorted by age:
users.sort(key=lambda u: u.age)
print(users)