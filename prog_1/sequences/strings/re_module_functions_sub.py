import re

text = """
<div>
    <label for="fname">First name:</label>
    <input id="fname" type="text">
</div>
<div>
    <label for="lname">First name:</label>
    <input id="lname" type="text">
</div>
"""

print(re.sub(r'([a-z])name', r'\1-name', text))


number_mapping = {'1': 'one',
                  '2': 'two',
                  '3': 'three'}
s = "1 testing 2 3"

print(re.sub(r'\d', lambda x: number_mapping[x.group()], s))
