import re


# \W Matches Unicode non word characters, neither alphanumeric in the current locale
# nor the underscore
# For  ASCII this becomes the equivalent of [^a-zA-Z0-9_]

re.split(r'\W+', 'Words, words, words')

# If capturing parentheses are used in pattern, then the text of all groups in the pattern are
# also returned as part of the resulting list.
re.split(r'(\W+)', 'Words, words, words')

# If maxsplit is nonzero, at most maxsplit splits occur, and the remainder of the string is
# returned as the final element of the list.
re.split(r'\W+', 'Words, words, words', 1)


re.split('[a-z]+', '0a3b9', flags=re.IGNORECASE)


re.split('(@)', 'roberto.vazquez@ufv.es')