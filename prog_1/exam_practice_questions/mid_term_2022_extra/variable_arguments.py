selection = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97}


def organize_primes(*numbers: int) -> dict:
    """
    Splits numbers in two lists of prime and non prime numbers.
    :param numbers: integers to split
    :returns: Returns a dictionary with two keys, prime and not-prime.
    """
    d = {'prime': [], 'not-prime': []}
    for n in numbers:
        if n in selection:
            d['prime'].append(n)
        else:
            d['not-prime'].append(n)
    return d


organize_primes(1, 2, 3, 4, 5, 6, 10, 41, 42)