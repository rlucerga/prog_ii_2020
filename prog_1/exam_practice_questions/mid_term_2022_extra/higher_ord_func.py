def add_tag(tag: str) -> callable:
    """Outputs a function that tags a text with a given HTML gat"""
    def inner(text: str):
        return f'<{tag}>{text}</{tag}>'

    return inner


tagger = add_tag('h1')
tagger('A header')
tagger = add_tag('p')
tagger('A paragraph')
