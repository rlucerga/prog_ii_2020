import datetime as dt


class TranscationRecords:
    def __init__(self):
        self._last_edit: dt.date = dt.datetime.today()
        self._transactions: list[dict[str, int]] = []  # Outer dict

    def add_tansaction(self, products: dict[str, int]):
        self._transactions.append(products)
        self._last_edit = dt.datetime.now()

    def get_last_edit(self) -> dt.date:
        return self._last_edit

    def get_quantities(self, query_sku: str) -> int:
        return sum(q
                   for entry in self._transactions
                   for sku, q in entry.items()
                   if sku == query_sku)

    def get_product_set(self, transaction_num: int) -> set:
        return set(self._transactions[transaction_num].keys())


t = TranscationRecords()
t.add_tansaction({'accupril': 1, 'skelaxin': 3, 'lipitor': 6})
t.add_tansaction({'genotropin': 1, 'skelaxin': 8})
t.add_tansaction({'lipitor': 7, 'accupril': 3})
t.get_quantities('accupril') # 4
t.get_product_set(0) # {'accupril', 'skelaxin'}



