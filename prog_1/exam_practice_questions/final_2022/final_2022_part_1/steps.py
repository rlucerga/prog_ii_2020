import uuid


class RouteStep:
    def __init__(self, stop_idx: int, address: str):
        self.stop_idx = stop_idx
        self.address = address


class Route:
    def __init__(self):
        self._stop_idx = 0
        self._route_id = uuid.uuid4()
        self._steps: list[RouteStep] = []

    def add_step(self, address: str):
        step = RouteStep(self._stop_idx, address)
        self._steps.append(step)
        self._stop_idx += 1

    def get_step(self, stop_idx: int):
        step = self._steps[stop_idx]
        return f'route: {self._route_id} step: {step.stop_idx} at address {step.address}'


# derived class
r1 = Route()
r1.add_step('Vereda de Palacio 1')
r1.add_step('Vereda de Palacio 2')
print(r1.get_step(1))

# route: 74f4421b-7b99-4e8e-ba6d-8aa5ac7ff4cc step: 1 at address Vereda de Palacio 2

r2 = Route()
r2.add_step('Vereda de Palacio 3')
r2.add_step('Vereda de Palacio 4')
print(r2.get_step(0))

# route: 0e1594c6-6473-4896-9b55-39d7146658d5 step: 0 at address Vereda de Palacio 3
