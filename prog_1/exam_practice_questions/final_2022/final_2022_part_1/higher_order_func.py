from typing import Callable
from math import log


class LogError(ValueError):
    pass


def log_base(base: int) -> Callable[[float], float]:
    def inner(x: float):
        if x <= 0:
            raise LogError
        else:
            return log(x, base)

    return inner


l2 = log_base(2)
print(l2(8)) # devuelve 3.0, porque 2 ** 3 == 8

l10 = log_base(10)
print(l10(100)) # devuelve 2.0 porque 10 ** 2 == 100

try:
    l10(-3)
except LogError:
    print('bad imput')

"""
3.0
2.0
bad imput
"""
