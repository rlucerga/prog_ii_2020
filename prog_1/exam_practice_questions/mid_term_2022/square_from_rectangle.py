class Rectangle:
    instance_count = 0

    def __init__(self, width: float, height: float):
        Rectangle.instance_count += 1
        self._width = width
        self._height = height

    def area(self) -> float:
        return self._width * self._height


class Square(Rectangle):
    def __init__(self, size: int):
        super().__init__(size, size)


# derived class
obj_1 = Square(4)
obj_1.area()
obj_1 = Square(5)
obj_1.area()
Square.instance_count
