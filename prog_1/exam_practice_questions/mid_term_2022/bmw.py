from dataclasses import dataclass


@dataclass
class Vehicle:
    serial_number: str
    category: str
    engine: int
    model: str

    def print_motor(self):
        print(f'Caterogy: {self.category} with {self.engine} cc')

    @classmethod
    def bmw160(cls, serial_number: str) -> 'Vehicle':
        return cls(serial_number=serial_number,
                   category='1',
                   model='160',
                   engine=1600)

    @classmethod
    def bmw180(cls, serial_number: str) -> 'Vehicle':
        return cls(serial_number=serial_number,
                   category='1',
                   model='120',
                   engine=1800)


v1 = Vehicle.bmw160('12SLJE3')
v2 = Vehicle.bmw180('45DJW36')

