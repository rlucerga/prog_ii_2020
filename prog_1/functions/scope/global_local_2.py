# This generates an Exception, because global_var is defined in the function and
# therefore it is considered local, but in print, it is not defined

# It is, in general, bad practice to "shadow" with local variables variables from outer
# scope

def my_funciton():
    local_var = 2
    # print(global_var, local_var)
    global_var = 3


global_var = 1
my_funciton()
