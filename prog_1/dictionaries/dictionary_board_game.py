from collections import UserDict


class ConflictedMove(Exception):
    pass


class Board(UserDict):
    def __init__(self, w: int = 3, h: int = 3, to_move: str = 'x'):
        super().__init__({'x': set(), 'o': set()})
        self.to_move = to_move
        self.h = h
        self.w = w
        self.squares = {(i, j) for i in range(w) for j in range(h)}

    def legal_moves(self) -> set[tuple[int, int]]:
        moves = {coord for coord_set in self.values() for coord in coord_set}
        return self.squares - moves

    def mark(self, coord: tuple[int, int]):
        if coord in self.legal_moves():
            self[self.to_move].add(coord)
            if self.to_move == 'x':
                self.to_move = 'o'
            else:
                self.to_move = 'x'
        else:
            raise ConflictedMove

    def __missing__(self, key):
        print(f'The player {key} does not exist')

b = Board(h=4, w=3, to_move='x')
b.mark((0, 0))
try:
    b.mark((0, 0))
except ConflictedMove:
    print('Used square')