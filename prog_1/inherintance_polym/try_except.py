from typing import Optional

import email_validator as ev

class User:
    def __init__(self, name: str,
                 email: str = None):
        self.name = name
        if email is None:
            self._email = None
        else:
            self._email = self.validate(email)

    @staticmethod
    def validate(email: str) -> Optional[str]:
        try:
            # Validate.
            valid = ev.validate_email(email)

            # Update with the normalized form.
            v_email = valid.email
        except ev.EmailNotValidError as e:
            # email is not valid, exception message is human-readable
            print(str(e))
            return None

        return v_email


user_1 = User('Bob', 'rvazquez@innitium.com')
user_2 = User('Bob', 'rvazquez@innitium')



