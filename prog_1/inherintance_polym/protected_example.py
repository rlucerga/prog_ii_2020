# Modified from https://www.geeksforgeeks.org/protected-variable-in-python/

class MainWarehouse:
    def __init__(self):
        # protected data members
        self._name = "Main Warehouse"
        self._address = "Castellana 111, Madrid"
        self.store = {}

    # public member function
    def display_name_and_address(self):
        # accessing protected data members
        print("Name: ", self._name)
        print("Address: ", self._address)

    # creating objects of the class


warehouse = MainWarehouse()

# calling public member
# functions of the class
warehouse.display_name_and_address()
