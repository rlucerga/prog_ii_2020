import os
import sqlite3

from flask import Flask, current_app, send_from_directory


app = Flask(__name__)

@app.route('/favicon.ico', methods=('GET',))
def favicon():
    return send_from_directory(directory=os.path.join(current_app.root_path, 'static'),
                               filename='innitium-logo.png',
                               mimetype='image/png')
