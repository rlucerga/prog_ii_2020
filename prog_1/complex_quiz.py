import datetime as dt

class Invoice:

    def __init__(self, number: str, description: str, date: dt.date, total: float):
        self.number = number
        self.description = description
        self.date = date
        self.total = total

    @staticmethod
    def basic_cleaning(number):
        return Invoice(number, "Basic Cleaning", dt.date.today(), 6.00)

    @staticmethod
    def deep_cleaning(number):
        return Invoice(number, "Deep Cleaning", dt.date.today(), 8.00)


i_123 = Invoice.basic_cleaning("123")
i_456 = Invoice.deep_cleaning("456")